package com.angelmatic

import org.springframework.dao.DataIntegrityViolationException

class DispensaryController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [dispensaryInstanceList: Dispensary.list(params), dispensaryInstanceTotal: Dispensary.count()]
    }

    def create() {
        [dispensaryInstance: new Dispensary(params)]
    }

    def save() {
        def dispensaryInstance = new Dispensary(params)
        if (!dispensaryInstance.save(flush: true)) {
            render(view: "create", model: [dispensaryInstance: dispensaryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), dispensaryInstance.id])
        redirect(action: "show", id: dispensaryInstance.id)
    }

    def show(Long id) {
        def dispensaryInstance = Dispensary.get(id)
        if (!dispensaryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "list")
            return
        }

        [dispensaryInstance: dispensaryInstance]
    }

    def edit(Long id) {
        def dispensaryInstance = Dispensary.get(id)
        if (!dispensaryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "list")
            return
        }

        [dispensaryInstance: dispensaryInstance]
    }

    def update(Long id, Long version) {
        def dispensaryInstance = Dispensary.get(id)
        if (!dispensaryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (dispensaryInstance.version > version) {
                dispensaryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'dispensary.label', default: 'Dispensary')] as Object[],
                          "Another user has updated this Dispensary while you were editing")
                render(view: "edit", model: [dispensaryInstance: dispensaryInstance])
                return
            }
        }

        dispensaryInstance.properties = params

        if (!dispensaryInstance.save(flush: true)) {
            render(view: "edit", model: [dispensaryInstance: dispensaryInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), dispensaryInstance.id])
        redirect(action: "show", id: dispensaryInstance.id)
    }

    def delete(Long id) {
        def dispensaryInstance = Dispensary.get(id)
        if (!dispensaryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "list")
            return
        }

        try {
            dispensaryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dispensary.label', default: 'Dispensary'), id])
            redirect(action: "show", id: id)
        }
    }
}
