package com.angelmatic

/**
 * Domain representation of a specific strain price
 * @author travis.roberson
 *
 */
class Price {

	static auditable = true
	boolean active
	Quantity quantity
	float price
	boolean specialPrice
	
	static belongsTo = [ menuItem : MenuItem ]
	
    static constraints = {
    }
}
