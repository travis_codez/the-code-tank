package com.angelmatic

class Strain {

	static auditable = true
	String name
	String code
	String description
	String lineage
	Type type
	Creator creator
	Integer rating
	String imageUrl
	boolean active = true
	
	
	
    static constraints = {
		
		name maxSize:255, nullable:false
		code unique:true, nullable:false
		description maxSize:500, nullable:true
		type nullable:false
		creator nullable:true
		rating range : 0..10

    }
}
