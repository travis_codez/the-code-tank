package com.angelmatic


/**
 * Domain object for dispensary address
 * @author travis.roberson
 *
 */

class Address {

	static auditable = true
	
	boolean active
	String address1
	String address2
	String city
	String state
	String zip
	
    static constraints = {
		address1 nullable:false
		city nullable:false
		state nullable:false
		zip nullable: false
    }
}
