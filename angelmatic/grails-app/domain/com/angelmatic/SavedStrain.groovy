package com.angelmatic

/**
 * A user bookmarked strain with personal rating and comments
 * @author troberson
 *
 */
class SavedStrain {

	static auditable = true
	Strain strain
	Integer rating
	String comments
	
	static belongsTo = [ account : Account]
	
    static constraints = {
		
		strain nullable:false
		rating nullable:false, range : 0..10
		comments maxSize:500

    }
}
