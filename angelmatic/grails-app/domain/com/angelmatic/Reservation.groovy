package com.angelmatic

/**
 * Domain object representation of a reservation
 * @author travis.roberson
 *
 */
class Reservation {

	static auditable = true
	
	Integer reservationNumber //TODO: Determine best approach to generate res number
	Date reservationDate
	float reservationTotal
	Dispensary dispensary	
	String contactEmail
	String contactPhone
	Date requestedTime
	boolean forDelivery
	String address1
	String address2
	String city
	String state
	String zip
	
	
	static belongsTo = [ account : Account]
	static hasMany = [ reservationItems : ReservationItem ]
	
    static constraints = {
		contactEmail email : true , nullable : false
    }
}
