package com.angelmatic

/**
 * Domain object for user account
 * @author travis.roberson
 *
 */

class Account {

	static auditable = true
	boolean active
	String first
	String middle
	String last
	String email
	String zipCode
	Integer birthDay
	Integer birthMonth
	Integer birthYear
	String resetCode
	Date resetRequestDate
	User user
	boolean mailingList = false
	boolean isCertified = false
	
	//boolean isRetailer = false
	//boolean isProducer = false
	//boolean isProcessor = false
	
	static hasMany = [ savedStrains : SavedStrain, reservations : Reservation, savedDispensaries : Dispensary]
	
    static constraints = {
		email email: true, nullable: false, blank: false, unique: true
		first nullable:false
		last nullable:false
		resetCode nullable :true
		resetRequestDate nullable:true
		
    }
}
