package com.angelmatic

/**
 * Domain object representation for a MenuItem
 * @author travis.roberson
 *
 */
class MenuItem {
	
	static auditable = true
	Strain strain
	boolean active
	Availability availability

	
	static hasMany = [ prices : Price]
	
    static constraints = {
    }
}
