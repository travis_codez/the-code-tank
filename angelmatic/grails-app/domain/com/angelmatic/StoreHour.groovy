package com.angelmatic

class StoreHour {
	

	Integer dayOfWeek
	Integer startHours
	Integer startMins

	Integer endHours
	Integer endMins
	
	
	private String formatTime(Integer hours, Integer mins) {
		String formattedHours = hours < 10 ? "0$hours" : hours.toString()
		String formattedMins = mins < 10 ? "0$mins" : mins.toString()
		"$formattedHours:$formattedMins"
	  }
	
	  String getStartTime() {
		formatTime(startHours, startMins)
	  }
	
	  String getEndTime() {
		formatTime(endHours, endMins)
	  }
	
	
	static belongsTo = [ dispensary : Dispensary ]
	
    static constraints = {
		
		dayOfWeek range: 0..6
		startHours range: 0..23
		endHours range: 0..23
	
		startMins range: 0..59
		endMins range: 0..59
		
    }
}
