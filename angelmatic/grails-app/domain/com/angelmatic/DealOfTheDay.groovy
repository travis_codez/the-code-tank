package com.angelmatic

class DealOfTheDay {

	static auditable = true
	boolean active
	boolean isExclusive
	Dispensary dispensary
	Strain strain
	String availability
	
	static transients = [ "availability"]
	
	
    static constraints = {
    }
}
