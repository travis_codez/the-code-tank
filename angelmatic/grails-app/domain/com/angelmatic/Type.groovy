package com.angelmatic

/**
 * Domain object for strain types
 * @author travis.roberson
 *
 */
class Type {

	static auditable = true
	String type
	
	
    static constraints = {
		type nullable:false
    }
}
