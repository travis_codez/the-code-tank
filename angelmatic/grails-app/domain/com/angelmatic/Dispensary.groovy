package com.angelmatic

/**
 * Domain class representation of a dispensary
 * @author travis.roberson
 *
 */

class Dispensary {
	
	static auditable = true
	String name
	String slogan
	String description
	Address address
	boolean delivery = false
	boolean hasOnlineReservation = false
	boolean hasMenu = false
	boolean featured = false
	boolean grow = false
	boolean flower = false
	boolean oil = false
	boolean food = false
	boolean utensils = false	
	String bannerImageUrl
	String email
	String phone
	Menu menu
	boolean active
	
	static hasMany = [ hours : StoreHour]
	

    static constraints = {	
		email email:true, nullable: false
		phone phone:true
		name maxSize:255, nullable: false
		description maxSize:1000, nullable: true
		phone nullable:false
		address nullable: false
		menu nullable:true
		
    }
}
