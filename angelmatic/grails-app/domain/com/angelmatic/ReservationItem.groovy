package com.angelmatic

/**
 * Domain object for Reservation Item
 * @author travis.roberson
 *
 */
class ReservationItem {
	
	Strain strain
	float qty
	float price
	
	static belongsTo = [ reservation : Reservation ]
	
    static constraints = {
    }
}
