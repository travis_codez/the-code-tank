package com.angelmatic

class Creator {

	static auditable = true
	String name
	String phone
	String email

    static constraints = {
		name maxSize:255, nullable:false
		email email : true
    }
}
