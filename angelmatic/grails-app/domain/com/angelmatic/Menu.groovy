package com.angelmatic

class Menu {
	
	static auditable = true
	boolean active
	static hasMany = [ menuItems : MenuItem ]
	static belongsTo = [ dispensary : Dispensary ]

    static constraints = {
    }
}
