import java.util.Date;

import com.angelmatic.*

import grails.util.GrailsUtil


class BootStrap {

    def init = { servletContext ->
		
		
		switch (GrailsUtil.environment) {
			case "production":
				break
			default:
				createTestData()
		}
		
		
		
    }
    def destroy = {
    }
	
	
	
	def createTestData() {
		
		
		def role = new Role(authority: 'ROLE_ADMIN').save()
		
		def user = User.findByUsername('tnorth22@gmail.com') ?: new User(
				username: 'tnorth22@gmail.com',
				password: 'Meekvc12',
				enabled: true).save(failOnError: true)
		
		UserRole.create(user, role).save()
		
		def account = Account.findByEmail('tnorth22@gmail.com') ?: new Account (
			active: true,
			first: 'Travis',
			middle: 'A',
			last: 'Roberson',
			email: 'tnorth22@gmail.com',
			birthDay: 3,
			birthMonth: 7,
			birthYear: 1982,
			zipCode: '98040',
			user: user
		).save(failOnError: true)
		
		
		
		def disAddress = new Address (
			active: true,
			address1: "3700 SE West Way",
			address2: "Suite 203",
			city: "Seattle",
			state: "WA",
			zip: "98029"
		).save(failOnError:true)
		
	
		def type = new Type (
			type: "Sativa"
		).save()
		
		
		def creator = new Creator (

			name:"Johnson Farms",
			phone: "(206) 552-9027",
			email: "test@test.com"
		).save()
		
		
		def strain = new Strain (
			
			name:"Train Wreck",
			code:"TRW",
			description: "Some description goes here",
			lineage: "Something x Something",
			type: type,
			creator:creator,
			rating: 8,
			imageUrl: "angelmatic/static/images/Marijuana-leaf-281x300.png"
		
		).save(failOnError:true)
			
	
		
		def avail = new Availability (
			availability:"Lots"
		).save(failOnError:true)
		
		
	
		
		
		
		def dispensary = new Dispensary (
			name:"The Great Dispensary",
			slogan:"When only the best will do.",
			description:"Some description about our super amazing product",
			address: disAddress,
			bannerImageUrl: "/foo/test/blah",
			email: "test@email.com",
			phone: "(206) 382-2838",
			active: true
		).save(failOnError:true)
		
		
		
		def menuItem = new MenuItem (
			strain:strain,
			availability:avail,
			active:true
		).save(failOnError:true)
		
		def menu = new Menu (
			active:true,
			dispensary:dispensary
		).save(failOnError:true)
		
		
		menu.addToMenuItems(menuItem)
		
		
		dispensary.menu = menu
			
		def dealOfTheDay = new DealOfTheDay (
			active:true,
			isExclusive:true,
			dispensary:dispensary,
			strain:strain
		).save(failOnError:true)
		
		
		
	}
	
	
	
}
