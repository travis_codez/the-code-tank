package com.angelmatic

class DealOfTheDayService {

	/**
	 * Get featured Deal of the Day
	 * @return
	 */
    def getFeatured() {	
		def featured = DealOfTheDay.findByActiveAndIsExclusive(true,true)	
		
		def menuItem
		if(featured) {
			def items = featured.dispensary.menu.menuItems
			menuItem = items.find( { t-> t.strain.id == featured.strain.id })
		}
		featured.availability = menuItem.availability.availability
		featured

    }
}
