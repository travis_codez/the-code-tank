
<%@ page import="com.angelmatic.Dispensary" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dispensary.label', default: 'Dispensary')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-dispensary" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-dispensary" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="email" title="${message(code: 'dispensary.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="phone" title="${message(code: 'dispensary.phone.label', default: 'Phone')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'dispensary.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'dispensary.description.label', default: 'Description')}" />
					
						<th><g:message code="dispensary.address.label" default="Address" /></th>
					
						<g:sortableColumn property="active" title="${message(code: 'dispensary.active.label', default: 'Active')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${dispensaryInstanceList}" status="i" var="dispensaryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${dispensaryInstance.id}">${fieldValue(bean: dispensaryInstance, field: "email")}</g:link></td>
					
						<td>${fieldValue(bean: dispensaryInstance, field: "phone")}</td>
					
						<td>${fieldValue(bean: dispensaryInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: dispensaryInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: dispensaryInstance, field: "address")}</td>
					
						<td><g:formatBoolean boolean="${dispensaryInstance.active}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${dispensaryInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
