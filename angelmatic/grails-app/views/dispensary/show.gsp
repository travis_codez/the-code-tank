
<%@ page import="com.angelmatic.Dispensary" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'dispensary.label', default: 'Dispensary')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-dispensary" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-dispensary" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list dispensary">
			
				<g:if test="${dispensaryInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="dispensary.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${dispensaryInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.phone}">
				<li class="fieldcontain">
					<span id="phone-label" class="property-label"><g:message code="dispensary.phone.label" default="Phone" /></span>
					
						<span class="property-value" aria-labelledby="phone-label"><g:fieldValue bean="${dispensaryInstance}" field="phone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="dispensary.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${dispensaryInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="dispensary.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${dispensaryInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="dispensary.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="address" action="show" id="${dispensaryInstance?.address?.id}">${dispensaryInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.active}">
				<li class="fieldcontain">
					<span id="active-label" class="property-label"><g:message code="dispensary.active.label" default="Active" /></span>
					
						<span class="property-value" aria-labelledby="active-label"><g:formatBoolean boolean="${dispensaryInstance?.active}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.bannerImageUrl}">
				<li class="fieldcontain">
					<span id="bannerImageUrl-label" class="property-label"><g:message code="dispensary.bannerImageUrl.label" default="Banner Image Url" /></span>
					
						<span class="property-value" aria-labelledby="bannerImageUrl-label"><g:fieldValue bean="${dispensaryInstance}" field="bannerImageUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.delivery}">
				<li class="fieldcontain">
					<span id="delivery-label" class="property-label"><g:message code="dispensary.delivery.label" default="Delivery" /></span>
					
						<span class="property-value" aria-labelledby="delivery-label"><g:formatBoolean boolean="${dispensaryInstance?.delivery}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.featured}">
				<li class="fieldcontain">
					<span id="featured-label" class="property-label"><g:message code="dispensary.featured.label" default="Featured" /></span>
					
						<span class="property-value" aria-labelledby="featured-label"><g:formatBoolean boolean="${dispensaryInstance?.featured}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.flower}">
				<li class="fieldcontain">
					<span id="flower-label" class="property-label"><g:message code="dispensary.flower.label" default="Flower" /></span>
					
						<span class="property-value" aria-labelledby="flower-label"><g:formatBoolean boolean="${dispensaryInstance?.flower}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.food}">
				<li class="fieldcontain">
					<span id="food-label" class="property-label"><g:message code="dispensary.food.label" default="Food" /></span>
					
						<span class="property-value" aria-labelledby="food-label"><g:formatBoolean boolean="${dispensaryInstance?.food}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.grow}">
				<li class="fieldcontain">
					<span id="grow-label" class="property-label"><g:message code="dispensary.grow.label" default="Grow" /></span>
					
						<span class="property-value" aria-labelledby="grow-label"><g:formatBoolean boolean="${dispensaryInstance?.grow}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.hasMenu}">
				<li class="fieldcontain">
					<span id="hasMenu-label" class="property-label"><g:message code="dispensary.hasMenu.label" default="Has Menu" /></span>
					
						<span class="property-value" aria-labelledby="hasMenu-label"><g:formatBoolean boolean="${dispensaryInstance?.hasMenu}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.hasOnlineReservation}">
				<li class="fieldcontain">
					<span id="hasOnlineReservation-label" class="property-label"><g:message code="dispensary.hasOnlineReservation.label" default="Has Online Reservation" /></span>
					
						<span class="property-value" aria-labelledby="hasOnlineReservation-label"><g:formatBoolean boolean="${dispensaryInstance?.hasOnlineReservation}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.hours}">
				<li class="fieldcontain">
					<span id="hours-label" class="property-label"><g:message code="dispensary.hours.label" default="Hours" /></span>
					
						<g:each in="${dispensaryInstance.hours}" var="h">
						<span class="property-value" aria-labelledby="hours-label"><g:link controller="storeHour" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.menu}">
				<li class="fieldcontain">
					<span id="menu-label" class="property-label"><g:message code="dispensary.menu.label" default="Menu" /></span>
					
						<span class="property-value" aria-labelledby="menu-label"><g:link controller="menu" action="show" id="${dispensaryInstance?.menu?.id}">${dispensaryInstance?.menu?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.oil}">
				<li class="fieldcontain">
					<span id="oil-label" class="property-label"><g:message code="dispensary.oil.label" default="Oil" /></span>
					
						<span class="property-value" aria-labelledby="oil-label"><g:formatBoolean boolean="${dispensaryInstance?.oil}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.slogan}">
				<li class="fieldcontain">
					<span id="slogan-label" class="property-label"><g:message code="dispensary.slogan.label" default="Slogan" /></span>
					
						<span class="property-value" aria-labelledby="slogan-label"><g:fieldValue bean="${dispensaryInstance}" field="slogan"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${dispensaryInstance?.utensils}">
				<li class="fieldcontain">
					<span id="utensils-label" class="property-label"><g:message code="dispensary.utensils.label" default="Utensils" /></span>
					
						<span class="property-value" aria-labelledby="utensils-label"><g:formatBoolean boolean="${dispensaryInstance?.utensils}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${dispensaryInstance?.id}" />
					<g:link class="edit" action="edit" id="${dispensaryInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
