<%@ page import="com.angelmatic.Dispensary" %>



<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="dispensary.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" value="${dispensaryInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'phone', 'error')} ">
	<label for="phone">
		<g:message code="dispensary.phone.label" default="Phone" />
		
	</label>
	<g:textField name="phone" value="${dispensaryInstance?.phone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="dispensary.name.label" default="Name" />
		
	</label>
	<g:textArea name="name" cols="40" rows="5" maxlength="255" value="${dispensaryInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="dispensary.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="1000" value="${dispensaryInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'address', 'error')} required">
	<label for="address">
		<g:message code="dispensary.address.label" default="Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="address" name="address.id" from="${com.angelmatic.Address.list()}" optionKey="id" required="" value="${dispensaryInstance?.address?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'active', 'error')} ">
	<label for="active">
		<g:message code="dispensary.active.label" default="Active" />
		
	</label>
	<g:checkBox name="active" value="${dispensaryInstance?.active}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'bannerImageUrl', 'error')} ">
	<label for="bannerImageUrl">
		<g:message code="dispensary.bannerImageUrl.label" default="Banner Image Url" />
		
	</label>
	<g:textField name="bannerImageUrl" value="${dispensaryInstance?.bannerImageUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'delivery', 'error')} ">
	<label for="delivery">
		<g:message code="dispensary.delivery.label" default="Delivery" />
		
	</label>
	<g:checkBox name="delivery" value="${dispensaryInstance?.delivery}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'featured', 'error')} ">
	<label for="featured">
		<g:message code="dispensary.featured.label" default="Featured" />
		
	</label>
	<g:checkBox name="featured" value="${dispensaryInstance?.featured}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'flower', 'error')} ">
	<label for="flower">
		<g:message code="dispensary.flower.label" default="Flower" />
		
	</label>
	<g:checkBox name="flower" value="${dispensaryInstance?.flower}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'food', 'error')} ">
	<label for="food">
		<g:message code="dispensary.food.label" default="Food" />
		
	</label>
	<g:checkBox name="food" value="${dispensaryInstance?.food}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'grow', 'error')} ">
	<label for="grow">
		<g:message code="dispensary.grow.label" default="Grow" />
		
	</label>
	<g:checkBox name="grow" value="${dispensaryInstance?.grow}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'hasMenu', 'error')} ">
	<label for="hasMenu">
		<g:message code="dispensary.hasMenu.label" default="Has Menu" />
		
	</label>
	<g:checkBox name="hasMenu" value="${dispensaryInstance?.hasMenu}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'hasOnlineReservation', 'error')} ">
	<label for="hasOnlineReservation">
		<g:message code="dispensary.hasOnlineReservation.label" default="Has Online Reservation" />
		
	</label>
	<g:checkBox name="hasOnlineReservation" value="${dispensaryInstance?.hasOnlineReservation}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'hours', 'error')} ">
	<label for="hours">
		<g:message code="dispensary.hours.label" default="Hours" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${dispensaryInstance?.hours?}" var="h">
    <li><g:link controller="storeHour" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="storeHour" action="create" params="['dispensary.id': dispensaryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'storeHour.label', default: 'StoreHour')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'menu', 'error')} required">
	<label for="menu">
		<g:message code="dispensary.menu.label" default="Menu" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="menu" name="menu.id" from="${com.angelmatic.Menu.list()}" optionKey="id" required="" value="${dispensaryInstance?.menu?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'oil', 'error')} ">
	<label for="oil">
		<g:message code="dispensary.oil.label" default="Oil" />
		
	</label>
	<g:checkBox name="oil" value="${dispensaryInstance?.oil}" />
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'slogan', 'error')} ">
	<label for="slogan">
		<g:message code="dispensary.slogan.label" default="Slogan" />
		
	</label>
	<g:textField name="slogan" value="${dispensaryInstance?.slogan}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: dispensaryInstance, field: 'utensils', 'error')} ">
	<label for="utensils">
		<g:message code="dispensary.utensils.label" default="Utensils" />
		
	</label>
	<g:checkBox name="utensils" value="${dispensaryInstance?.utensils}" />
</div>

