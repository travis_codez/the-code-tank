
<%@ page import="com.angelmatic.Strain" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'strain.label', default: 'Strain')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-strain" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-strain" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'strain.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="code" title="${message(code: 'strain.code.label', default: 'Code')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'strain.description.label', default: 'Description')}" />
					
						<th><g:message code="strain.type.label" default="Type" /></th>
					
						<th><g:message code="strain.creator.label" default="Creator" /></th>
					
						<g:sortableColumn property="rating" title="${message(code: 'strain.rating.label', default: 'Rating')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${strainInstanceList}" status="i" var="strainInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${strainInstance.id}">${fieldValue(bean: strainInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: strainInstance, field: "code")}</td>
					
						<td>${fieldValue(bean: strainInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: strainInstance, field: "type")}</td>
					
						<td>${fieldValue(bean: strainInstance, field: "creator")}</td>
					
						<td>${fieldValue(bean: strainInstance, field: "rating")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${strainInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
