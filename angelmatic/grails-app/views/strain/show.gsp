
<%@ page import="com.angelmatic.Strain" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'strain.label', default: 'Strain')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-strain" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-strain" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list strain">
			
				<g:if test="${strainInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="strain.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${strainInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.code}">
				<li class="fieldcontain">
					<span id="code-label" class="property-label"><g:message code="strain.code.label" default="Code" /></span>
					
						<span class="property-value" aria-labelledby="code-label"><g:fieldValue bean="${strainInstance}" field="code"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="strain.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${strainInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="strain.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:link controller="type" action="show" id="${strainInstance?.type?.id}">${strainInstance?.type?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="strain.creator.label" default="Creator" /></span>
					
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="creator" action="show" id="${strainInstance?.creator?.id}">${strainInstance?.creator?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.rating}">
				<li class="fieldcontain">
					<span id="rating-label" class="property-label"><g:message code="strain.rating.label" default="Rating" /></span>
					
						<span class="property-value" aria-labelledby="rating-label"><g:fieldValue bean="${strainInstance}" field="rating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.active}">
				<li class="fieldcontain">
					<span id="active-label" class="property-label"><g:message code="strain.active.label" default="Active" /></span>
					
						<span class="property-value" aria-labelledby="active-label"><g:formatBoolean boolean="${strainInstance?.active}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${strainInstance?.lineage}">
				<li class="fieldcontain">
					<span id="lineage-label" class="property-label"><g:message code="strain.lineage.label" default="Lineage" /></span>
					
						<span class="property-value" aria-labelledby="lineage-label"><g:fieldValue bean="${strainInstance}" field="lineage"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${strainInstance?.id}" />
					<g:link class="edit" action="edit" id="${strainInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
