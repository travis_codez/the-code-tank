<%@ page import="com.angelmatic.Strain" %>



<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="strain.name.label" default="Name" />
		
	</label>
	<g:textArea name="name" cols="40" rows="5" maxlength="255" value="${strainInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'code', 'error')} ">
	<label for="code">
		<g:message code="strain.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="code" value="${strainInstance?.code}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="strain.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="500" value="${strainInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="strain.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="type" name="type.id" from="${com.angelmatic.Type.list()}" optionKey="id" required="" value="${strainInstance?.type?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="strain.creator.label" default="Creator" />
		
	</label>
	<g:select id="creator" name="creator.id" from="${com.angelmatic.Creator.list()}" optionKey="id" value="${strainInstance?.creator?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'rating', 'error')} required">
	<label for="rating">
		<g:message code="strain.rating.label" default="Rating" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="rating" from="${0..10}" class="range" required="" value="${fieldValue(bean: strainInstance, field: 'rating')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'active', 'error')} ">
	<label for="active">
		<g:message code="strain.active.label" default="Active" />
		
	</label>
	<g:checkBox name="active" value="${strainInstance?.active}" />
</div>

<div class="fieldcontain ${hasErrors(bean: strainInstance, field: 'lineage', 'error')} ">
	<label for="lineage">
		<g:message code="strain.lineage.label" default="Lineage" />
		
	</label>
	<g:textField name="lineage" value="${strainInstance?.lineage}"/>
</div>

