<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<div id="wrap">
	
			<div id="topBanner" role="banner"><a href="http://grails.org"><img src="${resource(dir: 'images', file: 'grails_logo.png')}" alt="Grails"/></a>
			
			<div id="loginTopRight">
				<sec:ifLoggedIn>
							Welcome <sec:username/> | <g:link controller="logout" action="index">logout</g:link>			
				</sec:ifLoggedIn>
			
				<sec:ifNotLoggedIn>				
						Not Logged In ( <g:link controller="foo" action="bar"> Sign In</g:link> | <g:link controller="foo" action="bar" > Sign up</g:link> )
				</sec:ifNotLoggedIn>
			
				</div>
			
			</div>
			
				<div class="nav" role="navigation">
				<ul>
				<li><a href="foo">Home</a></li><li>|</li>
				<li><a href="foo">Dispensaries</a></li><li>|</li>
				<li><a href="foo">Catalog</a></li><li>|</li>
				<li><a href="foo">Deals of the Day</a></li><li>|</li>
				<li><a href="foo">MedSwap</a></li><li>|</li>
				<li><a href="foo">Settings</a></li>

			</ul>
		</div>
			
			
			<g:layoutBody/>

		</div>
		<div class="footer" role="contentinfo">

			<div class="footerText">
				<div class="footerSpace"></div>
				<a href="">Legal Disclosures</a> |
				<a href="">About Us</a> | 
				<a href="">FAQS & Information</a>
				<br>
				<a href="">Retailers</a> | 
				<a href="">Producers</a> | 
				<a href="">Processors</a>
				<br>
				<a href="">Profile</a>
			
			
			</div>
		
		</div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
