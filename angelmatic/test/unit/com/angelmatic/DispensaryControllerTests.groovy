package com.angelmatic



import org.junit.*
import grails.test.mixin.*

@TestFor(DispensaryController)
@Mock(Dispensary)
class DispensaryControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/dispensary/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.dispensaryInstanceList.size() == 0
        assert model.dispensaryInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.dispensaryInstance != null
    }

    void testSave() {
        controller.save()

        assert model.dispensaryInstance != null
        assert view == '/dispensary/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/dispensary/show/1'
        assert controller.flash.message != null
        assert Dispensary.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/dispensary/list'

        populateValidParams(params)
        def dispensary = new Dispensary(params)

        assert dispensary.save() != null

        params.id = dispensary.id

        def model = controller.show()

        assert model.dispensaryInstance == dispensary
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/dispensary/list'

        populateValidParams(params)
        def dispensary = new Dispensary(params)

        assert dispensary.save() != null

        params.id = dispensary.id

        def model = controller.edit()

        assert model.dispensaryInstance == dispensary
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/dispensary/list'

        response.reset()

        populateValidParams(params)
        def dispensary = new Dispensary(params)

        assert dispensary.save() != null

        // test invalid parameters in update
        params.id = dispensary.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/dispensary/edit"
        assert model.dispensaryInstance != null

        dispensary.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/dispensary/show/$dispensary.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        dispensary.clearErrors()

        populateValidParams(params)
        params.id = dispensary.id
        params.version = -1
        controller.update()

        assert view == "/dispensary/edit"
        assert model.dispensaryInstance != null
        assert model.dispensaryInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/dispensary/list'

        response.reset()

        populateValidParams(params)
        def dispensary = new Dispensary(params)

        assert dispensary.save() != null
        assert Dispensary.count() == 1

        params.id = dispensary.id

        controller.delete()

        assert Dispensary.count() == 0
        assert Dispensary.get(dispensary.id) == null
        assert response.redirectedUrl == '/dispensary/list'
    }
}
